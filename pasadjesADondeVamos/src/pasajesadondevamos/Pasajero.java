package pasajesadondevamos;

public class Pasajero {

    //Pasajero (dni, pasaporte, edad por si es menor, en tal caso que pida las autorizaciones)
    int dni;
    String pasaporte;
    int edad;
    String nombre;

    void comprar() {
    }
    
    //validacion de los datos
    boolean siMenor(){
        boolean menor;
        if (this.edad <= 17) {
            menor = true;
        } else {
            menor = false;
        }
        return menor;
    }

    // PARA SOBREESCRIBIR LA SALIDA DEL OBJETO.
    //clic con el boton derecho
    //insert code
    //toString()
    //Nos abre una ventana de dialogo
    //Seleccionamos todos los atributos
    //Clic en boton generate
    @Override
    public String toString() {
        return "Pasajero{" + "dni=" + dni + ", pasaporte=" + pasaporte + ", edad=" + edad + ", nombre=" + nombre + '}';
    }
}
