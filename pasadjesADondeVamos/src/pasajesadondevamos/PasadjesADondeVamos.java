package pasajesadondevamos;

import java.util.ArrayList;





        //pasajes        
        //lugar de salida y destino (fechas y hora).
        //Origen
        //Asientos
        // micro o vuelo?
        //exterior o cabotaje?
        //pasaporte, dni
        //escalas
        //a edad por si es menor o adulto y en el caso de ser menor las autorizaciones
        //Pagos
        //Asientos
        //Categoria: ejecutivo, primera
        //////////////////////////////
        //////////////////////////////
        
        //Pasajero (dni, pasaporte, edad por si es menor, en tal caso que pida las autorizaciones)
        //Pasaje (origen y destino, con fecha y hora, pagos, precio)        
        //Pago (podria ser otra entidad)
public class PasadjesADondeVamos {

    public static void main(String[] args) {
        System.out.println("[...]");
        //instanciamos 1 (creamos) un objeto a partir del molde (clase)
        Pasajero miPasajero1 = new Pasajero();
        miPasajero1.nombre = "Dio Brando";
        miPasajero1.dni = 35777222;
        miPasajero1.edad = 29;
        miPasajero1.pasaporte = "35777222AR";        
        System.out.println(miPasajero1.siMenor());
        
        //Mostrar pasajero
        System.out.println(miPasajero1);
        
        //instanciamos 2 (creamos) un objeto a partir del molde (clase)
        Pasajero miPasajero2 = new Pasajero();
        miPasajero2.nombre = "Yanina Fenandez";
        miPasajero2.dni = 40555999;
        miPasajero2.edad = 16;
        miPasajero2.pasaporte = "40555999AR";
        System.out.println(miPasajero2.siMenor());
        
        //Mostrar pasajero
        System.out.println(miPasajero2);
        
        //instanciamos 3 (creamos) un objeto a partir del molde (clase)
        Pasajero miPasajero3 = new Pasajero();
        miPasajero3.nombre = "Hugo Gonzalez";
        miPasajero3.dni = 36444222;
        miPasajero3.pasaporte = "36444222AR";
        miPasajero3.edad = 45;
        System.out.println(miPasajero3.siMenor());
        
        //Mostrar pasajero
        System.out.println(miPasajero3);
        
        
        //Crear el ArrayList
        ArrayList listadoPasajeros = new ArrayList();
//        System.out.println(listadoPasajeros);
        listadoPasajeros.add(miPasajero1);
//        System.out.println(listadoPasajeros);
        listadoPasajeros.add(miPasajero2);
//        System.out.println(listadoPasajeros);
        listadoPasajeros.add(miPasajero3);
//        System.out.println(listadoPasajeros);
        
        //Recorrer el array
        System.out.println("El total de pasajeros es: " + listadoPasajeros.size());
        for (int i = 0; i < listadoPasajeros.size(); i++) {
            System.out.println("Registro: " + listadoPasajeros.get(i));
            
        }
        
        
        
        System.out.println("[OK]");
    }
    
}
